dune-localfunctions (2.10.0-1) unstable; urgency=medium

  * New upstream release 2.10
  * d/patches: Refreshed patches
  * Depend on DUNE 2.10
  * Added debian/watch file
  * d/control: Depend on pkgconf instead of deprecated pkgconfig
  * d/control: Bumped standards version to 4.7.0 (no changes)

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 14 Nov 2024 21:15:37 +0100

dune-localfunctions (2.10~pre20240905-1) experimental; urgency=medium

  * New upstream release (2.10~pre20240905).
  * d/patches: Refreshed patches
  * Depend on DUNE 2.10
  * Added debian/watch file

 -- Markus Blatt <markus@dr-blatt.de>  Tue, 22 Oct 2024 06:35:21 +0200

dune-localfunctions (2.9.0-2) unstable; urgency=medium

  * d/control: Added Markus Blatt as uploader (with consent of Ansgar)
  * d: Install moved architecture independent pkgconfig file correctly.
  * Upload to unstable

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 12 Jan 2023 23:15:36 +0100

dune-localfunctions (2.9.0-1) experimental; urgency=medium

  [ Markus Blatt ]
  * d/control: Depend on DUNE 2.9.
  * d/upstream: Added metadata file.
  * New upstream release (2.9.0).

  [ Ansgar ]
  * d/control: set Standards-Version to 4.6.2 (no changes).

 -- Ansgar <ansgar@debian.org>  Sat, 07 Jan 2023 23:46:33 +0100

dune-localfunctions (2.8.0-5) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Thu, 21 Oct 2021 18:40:46 +0200

dune-localfunctions (2.8.0-4) experimental; urgency=medium

  * Increase tolerance for test-orthonormal even more. This should
    also fix the build failure on armel.

 -- Ansgar <ansgar@debian.org>  Tue, 19 Oct 2021 22:19:02 +0200

dune-localfunctions (2.8.0-3) experimental; urgency=medium

  * d/patches: Increase tolerance for test-orthonormal even more.

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Mon, 18 Oct 2021 13:25:43 +0200

dune-localfunctions (2.8.0-2) experimental; urgency=medium

  * d/patches: Add patch to increase tolerance for a unit test

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Fri, 15 Oct 2021 16:07:35 +0200

dune-localfunctions (2.8.0-1) experimental; urgency=medium

  * New upstream release.
  * d/control: Set Architecture to "any"

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Fri, 08 Oct 2021 13:39:34 +0200

dune-localfunctions (2.8.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * debian/rules: Explicitly set buildsystem to CMake

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Fri, 20 Aug 2021 07:09:59 -0400

dune-localfunctions (2.7.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Mon, 11 Jan 2021 22:31:42 +0100

dune-localfunctions (2.7.1-1) experimental; urgency=medium

  *  New upstream release.

 -- Lisa Julia Nebel <lisa_julia.nebel@tu-dresden.de>  Thu, 07 Jan 2021 09:32:45 +0100

dune-localfunctions (2.7.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Wed, 15 Jul 2020 12:38:05 +0200

dune-localfunctions (2.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Use debhelper compat level 13.
  * Bumped Standards-Version to 4.5.0 (no changes).

 -- Ansgar <ansgar@debian.org>  Mon, 25 May 2020 16:17:26 +0200

dune-localfunctions (2.6.0-1) unstable; urgency=medium

  * New upstream release.
  * d/control: now requires cmake >= 3.1

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 03 Apr 2018 02:09:34 +0200

dune-localfunctions (2.6.0~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * d/control: update Vcs-* fields for move to salsa.debian.org
  * Bumped Standards-Version to 4.1.3 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 06 Jan 2018 20:43:33 +0100

dune-localfunctions (2.6~20171110-1) experimental; urgency=medium

  * New upstream snapshot.
  * dune.module: revert version to 2.6-git
    + new patch: 0001-Revert-bump-version-to-2.7-git-ci-skip.patch
  * d/control: add `Rules-Requires-Root: no`
  * libdune-localfunctions-doc: add Built-Using: doxygen (for jquery.js)
  * Bumped Standards-Version to 4.1.1 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 14 Nov 2017 19:30:00 +0100

dune-localfunctions (2.5.1-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depend on texlive-pictures instead of pgf. (Closes: #867079)
  * debian/copyright: Update URLs.
  * Bumped Standards-Version to 4.0.0 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 18 Jul 2017 12:08:08 +0200

dune-localfunctions (2.5.1~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * Remove patch (applied upstream).

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 17 Jun 2017 17:14:36 +0200

dune-localfunctions (2.5.0-2) unstable; urgency=medium

  * Increase ε used for comparisons. (Closes: #860641)
    + patch: 0001-Merge-branch-bugfix-increase-epsilon-for-i386-into-m.patch

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 20 Apr 2017 19:32:17 +0200

dune-localfunctions (2.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 18 Dec 2016 13:53:46 +0100

dune-localfunctions (2.5.0~rc2-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release candidate.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 05 Dec 2016 23:26:54 +0100

dune-localfunctions (2.5.0~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last DUNE upload.
  * Switch to CMake.
  * Bumped Standards-Version to 3.9.8 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 21 Nov 2016 20:37:11 +0100

dune-localfunctions (2.4.1-1) unstable; urgency=medium

  * New upstream release.
  * (Build-)Depend on last DUNE upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 29 Feb 2016 10:35:27 +0100

dune-localfunctions (2.4.1~rc2-1) experimental; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last DUNE upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 28 Feb 2016 14:00:10 +0100

dune-localfunctions (2.4.0-1) unstable; urgency=medium

  * New upstream release.
  * (Build-)Depend on last DUNE upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 25 Sep 2015 22:33:39 +0200

dune-localfunctions (2.4~20150912rc3-1) unstable; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last DUNE upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 13 Sep 2015 14:06:29 +0200

dune-localfunctions (2.4~20150825rc2-1) experimental; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last DUNE upload.
  * libdune-localfunctions-dev: Add -doc package as a suggested package.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 04 Sep 2015 15:56:24 +0200

dune-localfunctions (2.4~20150717rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last DUNE upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 17 Jul 2015 21:16:51 +0200

dune-localfunctions (2.4~20150610gf860790-1) experimental; urgency=medium

  * New upstream snapshot.
  * debian/copyright: Update for new upstream snapshot.
  * (Build-)Depend on last DUNE upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 19 Jun 2015 23:50:21 +0200

dune-localfunctions (2.4~20150506g11a11a8-1) experimental; urgency=medium

  * New upstream snapshot.
  * debian/copyright: Update for new upstream snapshot.
  * Add Build-Depends-Indep: graphviz for "dot".
  * (Build-)Depend on last DUNE upload.
  * Install documentation to correct location.
  * Bumped Standards-Version to 3.9.6 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 06 May 2015 00:26:48 +0200

dune-localfunctions (2.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 17 Jun 2014 19:57:25 +0200

dune-localfunctions (2.3.1~rc1-1) experimental; urgency=medium

  * New upstream release candidate.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 10 Jun 2014 20:46:44 +0200

dune-localfunctions (2.3.0-2) unstable; urgency=medium

  * Install /usr/share/dune-localfunctions.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 14 Feb 2014 10:45:05 +0100

dune-localfunctions (2.3.0-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 14 Feb 2014 09:32:02 +0100

dune-localfunctions (2.3~20140117beta2-1) experimental; urgency=medium

  * New upstream snapshot.
  * libdune-localfunctions-dev.install: Install /usr/lib/cmake.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 20 Jan 2014 22:10:38 +0100

dune-localfunctions (2.3~20140110beta1-1) experimental; urgency=medium

  * New upstream snapshot.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 15 Jan 2014 13:43:42 +0100

dune-localfunctions (2.3~20131228gf8b14aa-1) experimental; urgency=medium

  * New upstream snapshot.
  * debian/control: Use canonical Vcs-* URIs.
  * Drop pre-depends on dpkg (>= 1.15.6) which is already satisfied in
    Debian 6.
  * Bumped Standards-Version to 3.9.5 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 01 Jan 2014 19:54:31 +0100

dune-localfunctions (2.2.1-2) unstable; urgency=low

  * Upload to unstable.
  * Bumped Standards-Version to 3.9.4 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 18 May 2013 19:18:12 +0200

dune-localfunctions (2.2.1-1) experimental; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 02 Mar 2013 02:36:01 +0100

dune-localfunctions (2.2.0-1) unstable; urgency=low

  * New upstream release.
  * Update debian/copyright.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 05 Jun 2012 10:34:45 +0200

dune-localfunctions (2.2~svn1056-1) experimental; urgency=low

  * New upstream snapshot.
  * debian/copyright: Update years of copyright.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 06 May 2012 15:20:25 +0200

dune-localfunctions (2.2~svn1043-1) experimental; urgency=low

  * Initial release. (Closes: #661823)

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 29 Mar 2012 20:15:03 +0200
